function is_email_valid(email){
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	return emailReg.test(email);
}


jQuery(document).ready(function($){

	$("form#attendees").on("submit", function(){
		var form = $(this);
		var errors = [];
		
		form.find(".required").each(function(){
			var val = $(this).val();
			if ($(this).attr("id") == 'email_address' && val != '' && !is_email_valid(val)) errors.push('Valid '+ $(this).siblings("label").text());
			else if (val == '') errors.push($(this).siblings("label").text());
		});
		form.find(".validate").each(function(){
			var val = $(this).val();
			if ($(this).attr("id") == 'image' && val != ''){
				var ext = val.split('.').pop().toLowerCase();
				if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
					errors.push('Valid '+ $(this).siblings("label").text());
				}
			}
		});
		
		if (errors.length > 0){
			var message = 'Please enter the following required fields:'+"\n\n";
			$.each(errors, function(idx, field){
				message += '- '+ field +"\n";
			});
			alert(message);
			return false;
		}
	});

});