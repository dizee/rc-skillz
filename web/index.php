<!doctype html>

<html lang="en">
<head>
<meta charset="utf-8">
<title>Rapid Crush Webinar Attendees</title>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>
<body>

<h1>Webinar Attendees</h1>

<?
$submitted = false;
if($_POST){
	$fname = filter_input(INPUT_POST, 'fname', FILTER_SANITIZE_STRING);
	$lname = filter_input(INPUT_POST, 'lname', FILTER_SANITIZE_STRING);
	$email_address = filter_input(INPUT_POST, 'email_address', FILTER_VALIDATE_EMAIL);
	$package = filter_input(INPUT_POST, 'package', FILTER_SANITIZE_STRING);
	$comments = filter_input(INPUT_POST, 'comments', FILTER_SANITIZE_STRING);
	$image = $_FILES['image']['name'];
	
	// if all required are ok, then the form shouldn't show anymore
	if ($fname and $lname and $email_address and $package){
		$submitted = true;
		$existing = false;
		
		// test without dupe error
		// $email_address = time();
		
		$mysqli = new mysqli('localhost', 'root', 'root', 'rapidcrush');
			
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		// check the database for a dupe email
		if ($stmt = $mysqli->prepare("SELECT email_address FROM skillz_webinar_attendees WHERE email_address = ?")){
			
			$stmt->bind_param("s", $email_address);

			$stmt->execute();

			$result = $stmt->get_result();
			
			if ($result->num_rows > 0){
				$existing = true;
				
				echo '<p>You have already entered your information. Thanks!';
			}
			// while ($row = $result->fetch_assoc()){
			// 	echo '<tr><td>'. $row['id'] .'</td><td>'. $row['value'] .'</td></tr>';
			// }
			
			$stmt->close();
		}
		
		// if no dupe, enter into db
		if (!$existing){
			$stmt = $mysqli->prepare("INSERT INTO skillz_webinar_attendees (fname, lname, email_address, package, comments, image) VALUES (?, ?, ?, ?, ?, ?)");
			// , comments = '?', image = 'x'
			$stmt->bind_param("ssssss", $fname, $lname, $email_address, $package, $comments, $image);
			$stmt->execute();
			
			// display thanks
			echo '<p>Thanks for your information!</p>';
			
			// and send email to support/sales
			$to = 'diannall44@gmail.com';
			$subject = 'Rapid Crush Webinar Attendee Submission';

			$headers  = 'MIME-Version: 1.0' . "\r\n";
			// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

			$headers .= 'From:diannall44@gmail.com' . "\r\n" .
			'Reply-To: diannall44@gmail.com' . "\r\n" .
			'X-Mailer: PHP/' . phpversion();

			$body = "First Name: $fname \n\n
Last Name: $lname \n\n
Email Address: $email_address \n\n
Comments: $comments \n\n
Image: $image \n\n";

			@mail($to, $subject, $body, $headers);
			
			$stmt->close();
		}

		$mysqli->close();
		
		
		
		
		
	}
}
?>

<? if (!$submitted): ?>
<form action="index.php" method="post" id="attendees" enctype="multipart/form-data">
<p><label for="fname">First Name</label> <input type="text" name="fname" id="fname" class="required"></p>
<p><label for="lname">Last Name</label> <input type="text" name="lname" id="lname" class="required"></p>
<p><label for="email_address">Email Address</label> <input type="text" name="email_address" id="email_address" class="required"></p>
<p><label for="package">Package</label> 
	<select name="package" id="package" size="1" class="required">
		<option value="">-</option>
		<option value="A">A</option>
		<option value="B">B</option>
		<option value="C">C</option>	
	</select>
</p>
<p><label for="comments">Comments</label><br><textarea name="comments" id="comments"></textarea></p>
<p><label for="image">Profile Image</label> <input type="file" name="image" id="image" class="validate"></p>
<p><input type="submit" value="Submit"></p>
</form>
<? endif; ?>

<script src="js/script.js"></script>
</body>
</html>