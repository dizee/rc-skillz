INSTALL:

Server reqs:
PHP 5.6
MySQL 5.6

Install table by running rapidcrush_2017-10-22.sql


NOTES & PLANNING:

I need to do image validation on the php side (currently only done by extension in JS)
Since this is profile image, I would like to add a cropper
Also need to upload file somewhere

I would also put a session cookie in there for future visits to the same form (currently not allowing dupe emails)

I would save this form as a template and build on it as needed.

Processing the form data would be done in a separate file and redirected to a thank you page to avoid reposting.

I would also add a human checker.